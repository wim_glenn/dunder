from functools import wraps


class MyString(str):
    def __contains__(self, substring):
        if substring == 'tat':
            return False
        return super(MyString, self).__contains__(substring)

thing = MyString('potato')


class GnarlyDict(dict):
    def __getitem__(self, item):
        if item == 'stop':
            return 'hammertime!!'
        return super(GnarlyDict, self).__getitem__(item)

    def __delitem__(self, item):
        if item == 'stop':
            raise Exception("can't touch this")
        super(GnarlyDict, self).__delitem__(item)


class HashableSlice(object):

    sentinel = object()  # to avoid hash collisions if we use "vanilla" tuples in the SlicableDict

    def __init__(self, *args):
        self._slice = slice(*args)
        self._hash = hash((self.start, self.stop, self.step, self.sentinel))

    # these are properties to enforce read-only
    @property
    def start(self):
        return self._slice.start
    @property
    def stop(self):
        return self._slice.stop
    @property
    def step(self):
        return self._slice.step

    def __eq__(self, other):
        if isinstance(other, (slice, HashableSlice)):
            # note:  unfortunately we're not allowed to inherit from slice, otherwise this would have been cleaner
            return (other.start, other.stop, other.step) == (self.start, self.stop, self.step)
        return False

    def __hash__(self):
        return self._hash

    def __repr__(self):
        return 'HashableSlice({0.start}, {0.stop}, {0.step})'.format(self)


def key_fix_decorator(f):
    """Method decorator for SlicableDict.
    If args[0] is a slice, make it into a HashableSlice"""
    # @wraps(f)  will only works here on python3 (because we decorate C functions)
    def wrapped(self, *args, **kwargs):
        if args:
            key = args[0]
            if isinstance(key, slice):
                key = HashableSlice(key.start, key.stop, key.step)
            args = (key,) + args[1:]
        return f(self, *args, **kwargs)
    wrapped.__name__ = getattr(f, '__name__', None)
    wrapped.__doc__ = getattr(f, '__doc__', None)    
    return wrapped


class SlicableDict(dict):
    pass

for method_name in '__setitem__', '__getitem__', '__delitem__', '__contains__', 'get', 'pop', 'setdefault':
    new_method = key_fix_decorator(getattr(SlicableDict, method_name))
    setattr(SlicableDict, method_name, new_method)
