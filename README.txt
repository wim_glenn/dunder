1) choose your level, easy medium or hard
2) read the exercises.  consider the unit tests as documentation.
3) write your code in "your_module.py"
4) run the command "make easy", "make medium", or "make hard" to test your implementation.  "make unit" runs them all
5) [optional] if you want a code review, feel free to add/commit your stuff and push it up in "your_module_yourname.py" 
