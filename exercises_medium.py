import random
from unittest import TestCase

from your_module import GnarlyDict


class MediumTest(TestCase):

    def test_its_a_dict(self):
        my_dict = GnarlyDict({0: 'hello'})
        self.assertIsInstance(my_dict, dict)
        self.assertEqual(my_dict[0], 'hello')
        my_dict[0] = 'world'
        self.assertEqual(my_dict[0], 'world')

    def test_key_stop_has_a_special_mapping(self):
        my_dict = GnarlyDict()
        self.assertEqual(my_dict['stop'], 'hammertime!!')

    def test_modifying_special_key_does_not_stick(self):
        my_dict = GnarlyDict({'stop': 'no'})
        self.assertEqual(my_dict['stop'], 'hammertime!!')
        my_dict['stop'] = 'this will be silently ignored'
        self.assertEqual(my_dict['stop'], 'hammertime!!')

    def test_special_value_untouchable(self):
        d = GnarlyDict()
        with self.assertRaises(Exception) as cm:
            del d['stop']
        self.assertEqual(cm.exception.message, "can't touch this")

    def test_deleting_other_stuff_still_works_normally(self):
        my_dict = GnarlyDict({x: str(x) for x in xrange(10)})
        key = random.choice([k for k in my_dict if k != 'stop'])
        self.assertIn(key, my_dict)
        del my_dict[key]
        self.assertNotIn(key, my_dict)
