from unittest import TestCase

from your_module import thing


class EasyTest(TestCase):

    def test_thing_compares_equal_to_string_potato(self):
        self.assertEqual(thing, 'potato')

    def test_thing_starts_with_pot(self):
        self.assertTrue(thing.startswith('pot'))

    def test_thing_does_not_contain_tat(self):
        self.assertFalse('tat' in thing)
