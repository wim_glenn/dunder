unit:
	python -m unittest exercises_easy
	python -m unittest exercises_medium
	python -m unittest exercises_hard

easy:
	python -m unittest exercises_easy

medium:
	python -m unittest exercises_medium

hard:
	python -m unittest exercises_hard
