import random
from unittest import TestCase

from your_module import SlicableDict


class HardTest(TestCase):

    def test_its_a_dict(self):
        my_dict = SlicableDict()
        self.assertIsInstance(my_dict, dict)

    def test_instantiate_with_copy(self):
        my_dict = SlicableDict({0: 'hello'})
        self.assertDictEqual(my_dict, {0: 'hello'})

    def test_instantiate_with_keyval_data(self):
        my_dict = SlicableDict(hello='world')
        self.assertDictEqual(my_dict, {'hello': 'world'})

    def test_set_slice_allowed(self):
        my_dict = SlicableDict()
        my_dict[1:3] = None

    def test_get_slice_allowed(self):
        my_dict = SlicableDict()
        my_dict[1:3] = None
        my_dict[1:3]

    def test_slices_stored_independently(self):
        my_dict = SlicableDict()

        my_dict[0] = 'a'
        my_dict[1] = 'z'
        my_dict[(0, 1)] = 'hello i am a tuple'
        my_dict[0:1] = 'a to z'

        self.assertEqual(my_dict[0], 'a')
        self.assertEqual(my_dict[1], 'z')
        self.assertEqual(my_dict[(0, 1)], 'hello i am a tuple')
        self.assertEqual(my_dict[0:1], 'a to z')

    def test_slice_containmentship(self):
        my_dict = SlicableDict()
        my_dict[0:10] = 'blah'
        self.assertIn(slice(0, 10), my_dict)

    def test_negative_slicing_allowed(self):
        my_dict = SlicableDict()
        my_dict[::-1] = 'potato'
        self.assertEqual(my_dict[::-1], 'potato')

    def test_deleting(self):
        my_dict = SlicableDict({0: 'hello'})
        my_dict[:] = 'i will be deleted'
        del my_dict[:]
        self.assertEqual(my_dict, {0: 'hello'})

    def test_popping(self):
        my_dict = SlicableDict({0: 'hello'})
        my_dict[:] = 'i will be deleted'
        x = my_dict.pop(slice(None))
        self.assertEqual(my_dict, {0: 'hello'})
        self.assertEqual(x, 'i will be deleted')

    def test_doesnt_create_dupes(self):
        my_dict = SlicableDict()
        thing = object()
        my_dict[1:2:3] = thing
        my_dict[slice(1, 2, 3)] = thing
        self.assertEqual(len(my_dict), 1)
        self.assertIs(my_dict.values()[0], thing)

    def test_reuse_the_key_the_dict_implemented_with(self):
        my_dict = SlicableDict()
        my_dict[1::2] = 'hello'

        k, = my_dict.keys()
        self.assertEqual(my_dict[k], 'hello')

        my_dict[k] = 123
        self.assertEqual(my_dict[k], 123)

    def test_slice_with_any_gnarly_object(self):
        my_dict = SlicableDict()
        my_dict['hello world':Ellipsis:random] = 'potato'
        s = slice('hello world', Ellipsis, random)
        self.assertEqual(my_dict[s], 'potato')

    def test_useful_repr(self):
        my_dict = SlicableDict()
        my_dict[3:] = 'spam'
        self.assertEqual(unicode(my_dict), u"{HashableSlice(3, None, None): 'spam'}")

    def test_get_slice_that_wasnt_set(self):
        my_dict = SlicableDict()

        with self.assertRaises(KeyError) as cm:
            my_dict[1:3]
        
        self.assertEqual(repr(cm.exception), 'KeyError(HashableSlice(1, 3, None),)')
        self.assertEqual(unicode(cm.exception), u'HashableSlice(1, 3, None)')
